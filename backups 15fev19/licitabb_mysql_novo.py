
# coding: utf-8

# In[13]:


# coding: utf-8

# IMPORTA BIBLIOTECAS E APIS


from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from twocaptchaapi import TwoCaptchaApi
from PIL import Image
from io import BytesIO
import os
from selenium.webdriver.chrome.options import Options
from datetime import date
import re

# Dados para o selenium funcionar no windows
options = Options()
options.headless = True
options.add_argument('--no-sandbox')  # Bypass OS security model
options.add_argument('--disable-gpu')  # applicable to windows os only
options.add_argument('start-maximized')  #
options.add_argument('disable-infobars')
options.add_argument("--disable-extensions")

api = TwoCaptchaApi('8508034575004a862e1aa4124a5d9428')

# inicia o webdriver -- headless mode
driver = webdriver.Chrome(options=options)

#Conecta ao mysql

import pymysql
import pymysql.cursors

connection = pymysql.connect(host="daudt.eng.br", user="braxi860_botBB", passwd="licitaBB", db="braxi860_licitadaudt")

if connection:
    print ("Conectado ao mysql!\n")

reiniciar = True

driver.get("https://google.com")


# IMPORTA O ARQUIVO DE CONFIGURAÇÃO


ListaMercadorias = [line.rstrip('\n') for line in open("mercadorias.txt", encoding='utf-8-sig')]
print(ListaMercadorias)




def entraemdetalhes(numlicitacao):
    # Abre página de acordo com o numero da licitação
    numerolicitacao = numlicitacao

    paginaDetalhamento = 'https://www.licitacoes-e.com.br/aop/consultar-detalhes-licitacao.aop?opcao=exibirDetalhesLicitacao&numeroLicitacao=' + numerolicitacao
    driver.get(paginaDetalhamento)

    # Captura googlekey

    googlekey = driver.find_element_by_xpath('//*[@id="html_element"]/div/div/iframe')
    googlekey = googlekey.get_property('src')
    s = googlekey
    googlekey = (re.search(r'&k=(.*?)&co', s).group(1))

    import requests

    apikey = '8508034575004a862e1aa4124a5d9428'
    requisicao = requests.post(
        'https://2captcha.com/in.php?key=' + apikey + '&method=userrecaptcha&googlekey=' + googlekey + '&pageurl=' + paginaDetalhamento)
    requisicao = requisicao.text.split('|')[1]

    # Tempo para a API resolver o recaptcha
    print("\nResolvendo o recaptcha")
    time.sleep(20)

    obteve = False

    while obteve == False:
        try:
            time.sleep(2)
            resposta = requests.get('https://2captcha.com/res.php?key=' + apikey + '&action=get&id=' + requisicao)
            resposta = resposta.text.split('|')[1]
            obteve = True
            print("Captcha Resolvido")
        except:
            obteve = False
            print("Tentando Resolver Captcha")
            pass

    driver.execute_script('document.getElementById("g-recaptcha-response").innerHTML="' + resposta + '";')

    # Enviando resposta
    driver.find_element_by_xpath('//*[@id="consultarDetalhesLicitacaoForm"]/input[3]').click()

    time.sleep(3)
    # Captura datas
    dt_abertura_propostas = driver.find_element_by_xpath('/html/body/fieldset/div[14]')
    dt_abertura_propostas = dt_abertura_propostas.text

    dt_inicio_acolhimento_propostas = driver.find_element_by_xpath('/html/body/fieldset/div[12]')
    dt_inicio_acolhimento_propostas = dt_inicio_acolhimento_propostas.text

    return(dt_abertura_propostas, dt_inicio_acolhimento_propostas)






def todooprograma(mercadoria):
    global reiniciar
    #DATA ATUAL
    data_atual = date.today()
    data_em_texto = data_atual.strftime('%d/%m/%Y')

    # driver recebe a página de busca e tenta até ela abrir
    carregou = False

    while carregou == False:
        print("Abindo pagina")
        driver.get("https://www.licitacoes-e.com.br/aop/pesquisar-licitacao.aop?opcao=preencherPesquisar")
        print("Abriu pagina")
        try:
            time.sleep(5)
            driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/a')
            carregou = True
            print("Carregou")
        except:
            carregou = False
            print("Erro de carregamento - tentando novamente...")
            pass



    time.sleep(5)
    # Seleciona o campo situação e o clica
    situacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/a')
    teste = situacao.click()

    # Insere valores inciais da situação da licitação, desce o cursor e aceita
    seletorSituacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/input')
    seletorSituacao.clear()
    seletorSituacao.send_keys("Publi")  # caracteres iniciais para 'publicadas'
    seletorSituacao.send_keys(Keys.ARROW_DOWN)
    seletorSituacao.send_keys(Keys.ENTER)
    
    #Seleciona período a partir de arquivo de configuração
    seletorPeriodo = ""
    for line in open("config_periodo.txt", encoding="utf-8"):
        li = line.strip()
        if not li.startswith("#"):
            seletorPeriodo = (line.rstrip())
    #seletorPeriodo

    if seletorPeriodo != "TUDO":
        # Seleciona o campo periodo e o clica
        situacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[8]/span/input')
        teste = situacao.click()

        # Insere valores inciais do periodo, desce o cursor e aceita
        seletorSituacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[8]/span/input')
        seletorSituacao.clear()

        if seletorPeriodo == "HOJE":
            seletorSituacao.send_keys("Hoj")  # caracteres iniciais
            seletorSituacao.send_keys(Keys.ARROW_DOWN)
            seletorSituacao.send_keys(Keys.ENTER)

        elif seletorPeriodo == "SEMANA_ATUAL":
            seletorSituacao.send_keys("Seman")  # caracteres iniciais
            seletorSituacao.send_keys(Keys.ARROW_DOWN)
            seletorSituacao.send_keys(Keys.ENTER)
        elif seletorPeriodo == "MES_ATUAL":
            seletorSituacao.send_keys("Mês")  # caracteres iniciais
            seletorSituacao.send_keys(Keys.ARROW_DOWN)
            seletorSituacao.send_keys(Keys.ENTER)
        else:
            pass

    # Insere o tipo de mercadoria a ser buscado
    campoMercadoria = driver.find_element_by_name("textoMercadoria")

    campoMercadoria.send_keys(mercadoria)
    
    #Insere o produto no campo mercadoria


    # Captura imagem do captcha

    ImagemCaptcha = driver.find_element_by_id("img_captcha")

    ImagemCaptcha = ImagemCaptcha.screenshot_as_png

    # Abre a imagem
    imagem = Image.open(BytesIO(ImagemCaptcha))

    # salva imagem temporária do captcha
    imagem.save("captcha.png")

    # In[194]:

    # resolve captcha

    with open('captcha.png', 'rb') as captcha_file:
        captcha = api.solve(captcha_file)
        print("Captcha enviado para API")
    captchaTexto = captcha.await_result()
    print("Captcha resolvido")

    # apaga imagem temporária
    os.remove("captcha.png")

    # In[195]:

    # insere a resposta do captcha
    campoCaptcha = driver.find_element_by_id("pQuestionAvancada")
    campoCaptcha.send_keys(captchaTexto)

    # Clica no botão de busca
    btnBusca = driver.find_element_by_name("pesquisar")
    btnBusca.click()



    # Tempo para a página carregar
    time.sleep(2)
    try:
        listaTudo = driver.find_element_by_name("tCompradores_length")
        Select(listaTudo).select_by_visible_text("Todos")
        print("Iniciando scrapping")

        reiniciar = False
    except:
        print("A página travou, reiniciando o script")

        reiniciar = True
        pass



    # Lista de compradores

    Compradores = driver.find_elements_by_xpath('//td[@width="25%"]')
    listaCompradores = []
    x = 0
    while x < len(Compradores):
        listaCompradores.append(Compradores[x].text)
        x = x + 1
        
    #Lista links
    

    # Lista de Num Licitação
    numLicitacao = driver.find_elements_by_xpath('//td[@width="10%"] [@valign="top"]')
    listaNumLicitacao = []
    x = 0
    while x < len(numLicitacao):
        listaNumLicitacao.append(numLicitacao[x].text)
        x = x + 1

    # Separa o detalhamento em listas de diversos elementos

    Detalhes = driver.find_elements_by_xpath('//td[@width="62%"] [@valign="top"]')

    listaObjeto = []
    listaUor = []
    listaUf = []
    listaModalidade = []
    listaNumEdital = []
    listaNumProcesso = []


    x = 0
    while x < len(Detalhes):
        textoSeparado = Detalhes[x].text.split('\n')

        Objeto = textoSeparado[0]
        Uor = textoSeparado[1].split('|')[0].split(':')[1].strip()
        Uf = textoSeparado[1].split('|')[1].split(':')[1].strip()
        Modalidade = textoSeparado[2].split(':')[1].strip()
        NumEdital = textoSeparado[3].split('|')[0].split(':')[1].strip()
        NumProcesso = textoSeparado[3].split('|')[1].split(':')[1].strip()

        listaObjeto.append(Objeto)
        listaUor.append(Uor)
        listaUf.append(Uf)
        listaModalidade.append(Modalidade)
        listaNumEdital.append(NumEdital)
        listaNumProcesso.append(NumProcesso)

        x = x + 1

    print("\n")


    #PRINT DAS LISTAS COM CONTEÚDO DAS LICITAÇÕES
    #AQUI SUBSTITUIR OS PRINTS PELA ENTRADA NA BASE DE DADOS
    
    

    
    ordem = "BB"
    uasg = "0"
    
    x=0
    while x < len(listaNumProcesso):
        
        print("Objeto e data?")
        print(listaObjeto[x])
        print(listaUor[x])
        print(listaUf[x])
        print(listaModalidade[x])
        print(listaNumEdital[x])
        print(listaNumProcesso[x])
        print(listaNumLicitacao[x])
        print("\n")
        DtAbertura_DtInicioAcolhimento = entraemdetalhes(listaNumLicitacao[x])
        print(DtAbertura_DtInicioAcolhimento)
        x = x+1
        link = "http://www.licitacoes-e.com.br/aop/consultar-detalhes-licitacao.aop?opcao=exibirDetalhesLicitacao&numeroLicitacao=" + str(listaNumLicitacao[x])
        
        mysql = "INSERT INTO licitacoes (ordem, link, objeto,localidade, licitacao, dt_abertura, uasg, uor ) VALUES ('" + ordem + "','" + link + "','" + listaObjeto[x] + "','" + listaUf[x] + "','" + listaNumLicitacao[x] +  "','" + str(DtAbertura_DtInicioAcolhimento[0]) +  "','" + uasg + "','" + listaUor[x] + "');"


        try:
            with connection.cursor() as cursor:
                sql = mysql
                cursor.execute(sql)
                result = cursor.fetchall()
                print(result)
        except Exception as e:
            print(e)
            pass



for mercadoria in ListaMercadorias:
    while reiniciar == True:
        print("reiniciando")
        todooprograma(mercadoria)

connection.close()

    #FIM DOS PRINTS


driver.quit()

