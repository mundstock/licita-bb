# coding: utf-8

# IMPORTA BIBLIOTECAS E APIS


from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
from twocaptchaapi import TwoCaptchaApi
from PIL import Image
from io import BytesIO
import os
from selenium.webdriver.chrome.options import Options
from datetime import date
import re

# Dados para o selenium funcionar no windows
options = Options()
options.headless = False
options.add_argument('--no-sandbox')  # Bypass OS security model
options.add_argument('--disable-gpu')  # applicable to windows os only
options.add_argument('start-maximized')  #
options.add_argument('disable-infobars')
options.add_argument("--disable-extensions")

api = TwoCaptchaApi('0dc59aa1cd32d477a810fc60f5a2556d')

# inicia o webdriver -- headless mode
driver = webdriver.Chrome(options=options)


reiniciar = True

def entraemdetalhes(numlicitacao):
    # Abre página de acordo com o numero da licitação
    numerolicitacao = numlicitacao

    paginaDetalhamento = 'https://www.licitacoes-e.com.br/aop/consultar-detalhes-licitacao.aop?opcao=exibirDetalhesLicitacao&numeroLicitacao=' + numerolicitacao
    driver.get(paginaDetalhamento)

    # Captura googlekey

    googlekey = driver.find_element_by_xpath('//*[@id="html_element"]/div/div/iframe')
    googlekey = googlekey.get_property('src')
    s = googlekey
    googlekey = (re.search(r'&k=(.*?)&co', s).group(1))

    import requests

    apikey = '0dc59aa1cd32d477a810fc60f5a2556d'
    requisicao = requests.post(
        'https://2captcha.com/in.php?key=' + apikey + '&method=userrecaptcha&googlekey=' + googlekey + '&pageurl=' + paginaDetalhamento)
    requisicao = requisicao.text.split('|')[1]

    # Tempo para a API resolver o recaptcha
    print("\nResolvendo o recaptcha")
    time.sleep(20)

    obteve = False

    while obteve == False:
        try:
            time.sleep(2)
            resposta = requests.get('https://2captcha.com/res.php?key=' + apikey + '&action=get&id=' + requisicao)
            resposta = resposta.text.split('|')[1]
            obteve = True
            print("Captcha Resolvido")
        except:
            obteve = False
            print("Tentando Resolver Captcha")
            pass

    driver.execute_script('document.getElementById("g-recaptcha-response").innerHTML="' + resposta + '";')

    # Enviando resposta
    driver.find_element_by_xpath('//*[@id="consultarDetalhesLicitacaoForm"]/input[3]').click()

    time.sleep(3)
    # Captura datas
    dt_abertura_propostas = driver.find_element_by_xpath('/html/body/fieldset/div[14]')
    dt_abertura_propostas = dt_abertura_propostas.text

    dt_inicio_acolhimento_propostas = driver.find_element_by_xpath('/html/body/fieldset/div[12]')
    dt_inicio_acolhimento_propostas = dt_inicio_acolhimento_propostas.text

    return(dt_abertura_propostas, dt_inicio_acolhimento_propostas)






def todooprograma():
    global reiniciar
    #DATA ATUAL
    data_atual = date.today()
    data_em_texto = data_atual.strftime('%d/%m/%Y')

    # driver recebe a página de busca e tenta até ela abrir
    carregou = False

    while carregou == False:
        driver.get("http://www.licitacoes-e.com.br/aop/pesquisar-licitacao.aop?opcao=preencherPesquisar")
        try:
            time.sleep(5)
            driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/a')
            carregou = True
            print("Carregou")
        except:
            carregou = False
            print("Erro de carregamento - tentando novamente...")
            pass



    time.sleep(5)
    # Seleciona o campo situação e o clica
    situacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/a')
    teste = situacao.click()

    # Insere valores inciais da situação da licitação, desce o cursor e aceita
    seletorSituacao = driver.find_element_by_xpath('//*[@id="licitacaoPesquisaSituacaoForm"]/div[5]/span/input')
    seletorSituacao.clear()
    seletorSituacao.send_keys("Publi")  # caracteres iniciais para 'publicadas'
    seletorSituacao.send_keys(Keys.ARROW_DOWN)
    seletorSituacao.send_keys(Keys.ENTER)



    # Captura imagem do captcha

    ImagemCaptcha = driver.find_element_by_id("img_captcha")

    ImagemCaptcha = ImagemCaptcha.screenshot_as_png

    # Abre a imagem
    imagem = Image.open(BytesIO(ImagemCaptcha))

    # salva imagem temporária do captcha
    imagem.save("captcha.png")

    # In[194]:

    # resolve captcha

    with open('captcha.png', 'rb') as captcha_file:
        captcha = api.solve(captcha_file)
        print("Captcha enviado para API")
    captchaTexto = captcha.await_result()
    print("Captcha resolvido")

    # apaga imagem temporária
    os.remove("captcha.png")

    # In[195]:

    # insere a resposta do captcha
    campoCaptcha = driver.find_element_by_id("pQuestionAvancada")
    campoCaptcha.send_keys(captchaTexto)

    # Clica no botão de busca
    btnBusca = driver.find_element_by_name("pesquisar")
    btnBusca.click()



    # Tempo para a página carregar
    time.sleep(2)
    try:
        listaTudo = driver.find_element_by_name("tCompradores_length")
        Select(listaTudo).select_by_visible_text("Todos")
        print("Iniciando scrapping")

        reiniciar = False
    except:
        print("A página travou, reiniciando o script")

        reiniciar = True
        pass



    # Lista de compradores

    Compradores = driver.find_elements_by_xpath('//td[@width="25%"]')
    listaCompradores = []
    x = 0
    while x < len(Compradores):
        listaCompradores.append(Compradores[x].text)
        x = x + 1

    # Lista de Num Licitação
    numLicitacao = driver.find_elements_by_xpath('//td[@width="10%"] [@valign="top"]')
    listaNumLicitacao = []
    x = 0
    while x < len(numLicitacao):
        listaNumLicitacao.append(numLicitacao[x].text)
        x = x + 1

    # Separa o detalhamento em listas de diversos elementos

    Detalhes = driver.find_elements_by_xpath('//td[@width="62%"] [@valign="top"]')

    listaObjeto = []
    listaUor = []
    listaUf = []
    listaModalidade = []
    listaNumEdital = []
    listaNumProcesso = []


    x = 0
    while x < len(Detalhes):
        textoSeparado = Detalhes[x].text.split('\n')

        Objeto = textoSeparado[0]
        Uor = textoSeparado[1].split('|')[0].split(':')[1].strip()
        Uf = textoSeparado[1].split('|')[1].split(':')[1].strip()
        Modalidade = textoSeparado[2].split(':')[1].strip()
        NumEdital = textoSeparado[3].split('|')[0].split(':')[1].strip()
        NumProcesso = textoSeparado[3].split('|')[1].split(':')[1].strip()

        listaObjeto.append(Objeto)
        listaUor.append(Uor)
        listaUf.append(Uf)
        listaModalidade.append(Modalidade)
        listaNumEdital.append(NumEdital)
        listaNumProcesso.append(NumProcesso)

        x = x + 1

    print("\n")


    #PRINT DAS LISTAS COM CONTEÚDO DAS LICITAÇÕES
    #AQUI VOCÊ PODE SUBSTITUIR OS PRINTS PELA ENTRADA EM UMA BASE DE DADOS


    #Aqui é importante fazer uma condição pra que rode apenas caso o numprocesso ou numlicitacao não exista no banco de dados


    x=0
    while x < len(listaNumProcesso):

        print(listaObjeto[x])
        print(listaUor[x])
        print(listaUf[x])
        print(listaModalidade[x])
        print(listaNumEdital[x])
        print(listaNumProcesso[x])
        print("\n")
        DtAbertura_DtInicioAcolhimento = entraemdetalhes(listaNumLicitacao[x])
        print(DtAbertura_DtInicioAcolhimento)
        x = x+1

    #FIM DOS PRINTS



    driver.quit()

while reiniciar == True:
    todooprograma()

